package mps;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Sample MCSS Client.
 */
public class MPS_MCSS_SampleClientCode {
    private static final boolean DEBUG_ENABLED = true;
    
    private static final String HOST_NAME = "http://l921m237.v30.amdocs.com:12820";

    private static final String USER_NAME = "LisaYork";

    private static final String PASSWORD = "Unix1234";

    private static final String ASSIGNED_PRODUCT_ID = "100169362";
    
    private static final String BAR_ID = "333333451";

    public static final String LOGIN_URL = "/mcss/Login";

    public static final String GET_USER_INFO_URL = "/mcss/utility/user_information";

    public static final String GET_CONTACT_PERSON_URL = "/mcss/customer/contact-person/{id}?lo=en_US&sc=SS";

    public static final String GET_ASSIGNED_PRODUCTS_URL = "/rp-server/ecommerce/customer/{customerId}/products?locale=en_US&sc=SS";

    public static final String GET_LAST_BILLS_URL = "/mcss/ebill/billing-arrangement/{barId}/bills?numOfCycles=12&locale=en_US&salesChannel=SS";

    public static final String ASSIGNED_PRODUCT_SERVICES_URL = "/rp-server/ecommerce/customer/{customerId}/product/{serviceId}?lo=en_US&sc=SS&allParams=true";

    public static final String AVAILABLE_SERVICES_URL = "/rp-server/ecommerce/customer/{customerId}/product/{productId}/add-ons?lo=en_US&sc=SS";

    public static final String PRODUCT_CONFIGURATION_URL = "/rp-server/commerce/customer/{customerId}/product/{productId}/product-configuration-options?sc=SS&locale=en_US";
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        try {
            MPS_MCSS_SampleClientCode mcssClient = new MPS_MCSS_SampleClientCode();
            String token = mcssClient.login(USER_NAME, PASSWORD);
            JSONObject userObj = mcssClient.getUserInfo(token);
            String personId = userObj.getJSONObject("ClientUserContext").getString("personId");
            String customerId = userObj.getJSONObject("ClientUserContext").getString("customerId");
            mcssClient.getCustomerDetails(token, personId);
            mcssClient.getAssignedProducts(token, customerId);
            mcssClient.getLastBills(token, BAR_ID);
            mcssClient.getAssignedProductServices(token, customerId, ASSIGNED_PRODUCT_ID);
            mcssClient.getAvailableService(token, customerId, ASSIGNED_PRODUCT_ID);
            mcssClient.getProductConfiguration(token, customerId, ASSIGNED_PRODUCT_ID);
        } catch (JSONException e) {
            System.err.println(e);
        }
    }

    /**
     * 
     * @param userName
     * @param password
     * @return 
     */
    public String login(String userName, String password) {
        System.out.println();
        System.out.println("*********************");
        System.out.println("* Login");
        System.out.println("*********************");
        
        String token = null;
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost post = new HttpPost(HOST_NAME + LOGIN_URL);
            JSONObject data = new JSONObject();
            JSONObject credentials = new JSONObject();
            credentials.put("user", userName);
            credentials.put("password", password);
            data.put("Credentials", credentials);
            StringEntity params = new StringEntity(data.toString());
            post.addHeader("content-type", "application/json");
            post.setEntity(params);
            if (DEBUG_ENABLED) {
                System.out.println(post);
            }
            HttpResponse response = client.execute(post);
            token = response.getFirstHeader("uxfauthorization").getValue();
            if (DEBUG_ENABLED) {
                System.out.println(response.getStatusLine());
                System.out.println("Authorization: " + token);
                System.out.println(readResponseBody(response));
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        return token;
    }
    
    /**
     * 
     * @param token
     * @return
     * @throws JSONException 
     */
    public JSONObject getUserInfo(String token) throws JSONException {
        System.out.println();
        System.out.println("*********************");
        System.out.println("* User Info");
        System.out.println("*********************");

        String responseBody = null;
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet get = new HttpGet(HOST_NAME + GET_USER_INFO_URL);
            get.addHeader("Authorization", token);
            if (DEBUG_ENABLED) {
                System.out.println(get);
            }            
            HttpResponse response = client.execute(get);
            responseBody = readResponseBody(response);            
            if (DEBUG_ENABLED) {
                System.out.println(response.getStatusLine());
                System.out.println(responseBody);
            }
        } catch (Exception e) {
            System.err.println(e);
        }

        return new JSONObject(responseBody);
    }
    
    /**
     * 
     * @param token
     * @param personId
     * @return
     * @throws JSONException 
     */
    public JSONObject getCustomerDetails(String token, String personId) throws JSONException {
        System.out.println();
        System.out.println("*********************");
        System.out.println("* Cusomer Details");
        System.out.println("*********************");

        String responseBody = null;
        try {
            HttpClient client = HttpClientBuilder.create().build();
            Map map = new HashMap();
            map.put("id", personId);
            map.put("sc", "SS");
            HttpGet get = new HttpGet(MapFormat.format(HOST_NAME + GET_CONTACT_PERSON_URL, map));
            get.addHeader("authorization", token);
            if (DEBUG_ENABLED) {
                System.out.println(get);
            }
            HttpResponse response = client.execute(get);
            responseBody = readResponseBody(response);
            if (DEBUG_ENABLED) {
                System.out.println(response.getStatusLine());
                System.out.println(responseBody);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        
        return new JSONObject(responseBody);
    }
    
    /**
     * 
     * @param token
     * @param customerId
     * @return
     * @throws JSONException 
     */
    public JSONObject getAssignedProducts(String token, String customerId) throws JSONException {
        System.out.println();
        System.out.println("*********************");
        System.out.println("* Assigned Products");
        System.out.println("*********************");

        String responseBody = null;
        try {
            HttpClient client = HttpClientBuilder.create().build();
            Map map = new HashMap();
            map.put("customerId", customerId);
            map.put("sc", "SS");
            HttpGet get = new HttpGet(MapFormat.format(HOST_NAME + GET_ASSIGNED_PRODUCTS_URL, map));
            get.addHeader("authorization", token);
            if (DEBUG_ENABLED) {
                System.out.println(get);
            }
            HttpResponse response = client.execute(get);
            responseBody = readResponseBody(response);
            if (DEBUG_ENABLED) {
                System.out.println(response.getStatusLine());
                System.out.println(responseBody);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        
        return new JSONObject(responseBody);
    }
    
    /**
     * 
     * @param token
     * @param barId
     * @return
     * @throws JSONException 
     */
    public JSONObject getLastBills(String token, String barId) throws JSONException {
        System.out.println();
        System.out.println("*********************");
        System.out.println("* Last Bills");
        System.out.println("*********************");

        String responseBody = null;
        try {
            HttpClient client = HttpClientBuilder.create().build();
            Map map = new HashMap();
            map.put("barId", barId);
            map.put("sc", "SS");
            HttpGet get = new HttpGet(MapFormat.format(HOST_NAME + GET_LAST_BILLS_URL, map));
            get.addHeader("authorization", token);
            if (DEBUG_ENABLED) {
                System.out.println(get);
            }
            HttpResponse response = client.execute(get);
            responseBody = readResponseBody(response);
            if (DEBUG_ENABLED) {
                System.out.println(response.getStatusLine());
                System.out.println(responseBody);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        
        return new JSONObject(responseBody);
    }
    
    /**
     * 
     * @param token
     * @param customerId
     * @param serviceId
     * @return
     * @throws JSONException 
     */
    public JSONObject getAssignedProductServices(String token, String customerId, String serviceId) throws JSONException {
        System.out.println();
        System.out.println("*********************");
        System.out.println("* Assigned Product Services");
        System.out.println("*********************");

        String responseBody = null;
        try {
            HttpClient client = HttpClientBuilder.create().build();
            Map map = new HashMap();
            map.put("customerId", customerId);
            map.put("serviceId", serviceId);
            HttpGet get = new HttpGet(MapFormat.format(HOST_NAME + ASSIGNED_PRODUCT_SERVICES_URL, map));
            get.addHeader("authorization", token);
            if (DEBUG_ENABLED) {
                System.out.println(get);
            }
            HttpResponse response = client.execute(get);
            responseBody = readResponseBody(response);
            if (DEBUG_ENABLED) {
                System.out.println(response.getStatusLine());
                System.out.println(responseBody);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        
        return new JSONObject(responseBody);
    }
    
    /**
     * 
     * @param token
     * @param customerId
     * @param productId
     * @return
     * @throws JSONException 
     */
    public JSONObject getAvailableService(String token, String customerId, String productId) throws JSONException {
        System.out.println();
        System.out.println("*********************");
        System.out.println("* Available Services");
        System.out.println("*********************");

        String responseBody = null;
        try {
            HttpClient client = HttpClientBuilder.create().build();
            Map map = new HashMap();
            map.put("customerId", customerId);
            map.put("productId", productId);
            HttpGet get = new HttpGet(MapFormat.format(HOST_NAME + AVAILABLE_SERVICES_URL, map));
            get.addHeader("authorization", token);
            if (DEBUG_ENABLED) {
                System.out.println(get);
            }
            HttpResponse response = client.execute(get);
            responseBody = readResponseBody(response);
            if (DEBUG_ENABLED) {
                System.out.println(response.getStatusLine());
                System.out.println(responseBody);
            }
        } catch (Exception e) {
            System.err.println(e);
        }

        return new JSONObject(responseBody);
    }
    
    /**
     * 
     * @param token
     * @param customerId
     * @param productId
     * @return
     * @throws JSONException 
     */
    public JSONObject getProductConfiguration(String token, String customerId, String productId) throws JSONException {
        System.out.println();
        System.out.println("*********************");
        System.out.println("* Product Configuration");
        System.out.println("*********************");
        
        String responseBody = null;
        try {
            HttpClient client = HttpClientBuilder.create().build();
            Map map = new HashMap();
            map.put("customerId", customerId);
            map.put("productId", productId);
            HttpPost post = new HttpPost(MapFormat.format(HOST_NAME + PRODUCT_CONFIGURATION_URL, map));
            post.addHeader("authorization", token);
            post.addHeader("content-type", "application/json");
            // Generate the payload
            JSONObject retrieveProductConfigurationRequest = new JSONObject();
            JSONObject innerJson = new JSONObject();
            JSONObject returnConfigurationDataJson = new JSONObject();
            retrieveProductConfigurationRequest.put("RetrieveProductConfigurationRequest", innerJson);
            innerJson.put("activityForConfiguration", JSONObject.NULL);
            innerJson.put("configurationLevel", "FULL");
            innerJson.put("settingsView", "FULL");
            innerJson.put("loadFirstStep", false);
            innerJson.put("stepIDs", new JSONArray());
            innerJson.put("containedProductSpecID", "8027812");
            innerJson.put("serviceRequiredDate", JSONObject.NULL);
            innerJson.put("containedProductID", JSONObject.NULL);
            innerJson.put("returnConfigurationData", returnConfigurationDataJson);
            returnConfigurationDataJson.put("catalogDataInd", true);
            returnConfigurationDataJson.put("crossProductsDiscountsDataInd", true);
            returnConfigurationDataJson.put("previousQuotedRecurringPricesInd", false);
            returnConfigurationDataJson.put("productConfigurationInd", false);
            returnConfigurationDataJson.put("quotedPricesInd", true);
            returnConfigurationDataJson.put("quotedProrationInd", false);
            returnConfigurationDataJson.put("quotedTaxInd", false);
            returnConfigurationDataJson.put("summaryInd", false);
            StringEntity params = new StringEntity(retrieveProductConfigurationRequest.toString());
            post.setEntity(params);
            if (DEBUG_ENABLED) {
                System.out.println(post);
            }
            HttpResponse response = client.execute(post);
            responseBody = readResponseBody(response);
            if (DEBUG_ENABLED) {
                System.out.println(response.getStatusLine());
                System.out.println(responseBody);
            }
        } catch (Exception e) {
            System.err.println(e);
        }

        return new JSONObject(responseBody);
    }

    /**
     * Utility method for reading the response body.
     * @param response
     * @return the body of the given response as a String object.
     * @throws UnsupportedOperationException
     * @throws IOException 
     */
    private String readResponseBody(HttpResponse response) throws UnsupportedOperationException, IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
        StringBuilder sb = new StringBuilder();
        String read;
        while ((read = br.readLine()) != null) {
            sb.append(read);
        }

        return sb.toString();
    }
}
